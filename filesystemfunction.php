<?php
$path = "/testweb/home.php";

//Show filename with file extension
echo basename($path) ."<br/>";

//Show filename without file extension
echo basename($path,".php");

echo "<br/>";
echo "<br/>";

echo dirname("c:/testweb/home.php") . "<br />";
echo dirname("/testweb/home.php");

echo "<br/>";
echo "<br/>";
//============================================================================
//existing file open
$file = fopen("file.txt","r");
var_dump($file) ;


echo "<br/>";
echo "<br/>";
//new file open
$file = fopen("newfile.txt","w");
var_dump($file) ;

echo "<br/>";
echo "<br/>";
//==========================================================================
//existing file open
$file = fopen("file.txt","r");
var_dump($file) ;

//existing file close
echo "<br/>";
echo "<br/>";
$file = fclose("file.txt");
var_dump($file) ;

echo "<br/>";
echo "<br/>";
//==========================================================================
//file_get_content read
echo file_get_contents("file.txt");
echo "<br/>";
echo "<br/>";
//==========================================================================
//file_put_content write
echo file_put_contents("file.txt","I am BITM Student");
echo "<br/>";
echo "<br/>";
//==========================================================================
//file_put_content write with FILE_APPEND
echo file_put_contents("file.txt","I am taking course on PHP",FILE_APPEND);

echo "<br/>";
echo "<br/>";
//==========================================================================
//The filesize() function returns the size of the specified file.
echo filesize("file.txt");
echo "<br/>";
echo "<br/>";
//==========================================================================
//The filetype() function returns the file type of a specified file or directory..
echo filetype("file.txt");
//one of folder of project
echo "<br/>";
echo "<br/>";
echo filetype("images");
echo "<br/>";
echo "<br/>";
//==========================================================================
//The fread() reads from an open file.
$file = fopen("file.txt","r");
echo fread($file,"17");

echo "<br/>";
echo "<br/>";
//==========================================================================
//The pathinfo() function returns an array that contains information about a path.
echo '<pre>';
print_r( pathinfo("file.txt"));
echo '</pre>';

echo "<br/>";
echo "<br/>";
//==========================================================================
//The unlink() function deletes a file.

$file = "newfile.txt";
if (unlink($file))
echo ("Deleted $file");
else
echo "failed";
